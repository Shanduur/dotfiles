alias l='ls -l --color=auto'
alias ls='ls --color=auto'
alias la='ls -a --color=auto'
alias ll='ls -la --color=auto'
alias grep='grep --color=auto'