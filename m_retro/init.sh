#!/bin/bash

set -e

DAPPS='/gsrc/dockapps'
GAMES='/media/games'

# This script will work only on Void Linux

sudo xbps-install unzip git wget gcc make automake autoconf neovim nano mc netsurf neofetch xterm xorg windowmaker

[ ! -d /gsrc ] && sudo mkdir /gsrc && chown $(whoami) /gsrc
cd /gsrc
git clone https://github.com/window-maker/dockapps dockapps && cd $DAPPS
# wmshutdown dockapp - shutdown machine from dock
cd $DAPPS/wmshutdown
./configure && make && sudo make install
# asmon - system resources monitor
cd $DAPPS/asmon/asmon
make && sudo make install

mkdir $GAMES
cd $GAMES
wget http://download.tuxfamily.org/openarena/rel/088/openarena-0.8.8.zip
unzip openarena-0.8.8.zip
rm -f *.zip